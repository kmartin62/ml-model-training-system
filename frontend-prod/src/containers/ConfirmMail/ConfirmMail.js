import React, {Component, PropTypes} from 'react';
import { useParams } from 'react-router-dom';
import { BASE_URL } from '../../helpers/dthelper';

export default class ConfirmMail extends Component{

  static propTypes = {
    id: PropTypes.string,
    element: PropTypes.string
  }

  constructor(props) {
    super(props);

    this.state = {
      errorMessage: "Your account has been verified"
    };
  }



  componentDidMount(){
    const requestOptions = {
      method: 'POST'
    };

    fetch(BASE_URL + 'confirm/' + this.props.params.id, requestOptions)
    .then(async response => {
      if(!response.ok){
        this.setState({errorMessage:"Oops, an error occured. Either your account is verified or the link you've provided has expired"})
      }
    }).catch(error => {
      alert("ERROR")
    })
  }

  render() {
    const errorResponse = this.state.errorMessage;

    return (
      <div className="container">
        {errorResponse}
      </div>

      );
  }
}
