/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import './UploadFile.css';
import { getUidToken } from '../../helpers/dthelper';

export default class UploadFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
        selectedFile: null
      };
    }

    sleep = (ms) => {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

      onFileChange = async event => {
        this.setState({selectedFile: event.target.files[0]});
        await this.sleep(1000);
        this.onFileUpload();
      };

      onFileUpload = () => {
        const URL = 'http://localhost:8000';
        const formData = new FormData();

        formData.append(
          "file",
          this.state.selectedFile,
          this.state.selectedFile.name,
        );

        if (this.props.toPredict === 0){

        fetch(URL + "/uploadfile?to_predict=" + this.props.toPredict, {
          method: "POST",
          body: formData
        })
        .then(result => {
          if (result.status !== 200) {
              this.setState({errMsg: 'Invalid file'});
          }

          if (result.status === 200) {
            this.setState({errMsg: ''})
             fetch(URL + "/columns")
            .then(resp => {
              return resp.json();
            })
            .then(resp => {
              console.log(resp.data);
              this.props.onUploadCSV(resp.data);
            });
          }
        });
      } else {
        fetch(URL + "/uploadfile?to_predict=" + this.props.toPredict, {
          method: "POST",
          body: formData
        })
        .then(result => {
          if (result.status !== 200) {
              this.setState({errMsg: 'Invalid file'});
          }

          if (result.status === 200) {
            this.setState({errMsg: ''})
             fetch(URL + "/predict?UID=" + getUidToken().uid + "&projectId=" + this.props.projectId)
            .then(resp => {
              return resp.json();
            })
            .then(resp => {
              this.props.getPredictions(resp.predicted_data);
            });
          }
        });
      }
      };


      render() {
        return (
          

          <div>
            <div>
              <input type="file" onChange={this.onFileChange} required style={{width: "225px", marginLeft: "136px"}}/>
            </div>
         </div>
        );
      }
}
