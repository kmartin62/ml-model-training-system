import React from 'react';
import "./HowTo.css";

export default class HowTo extends React.Component {
  render() {
  return (
    <ul className="cd-hero-slider">
    <li style={{backgroundColor: '#057a8d'}}>
                    <div className="cd-full-width" style={{marginTop: "-170px", marginLeft: "80px"}}>
                        <div className="container js-tm-page-content tm-section-margin-t" data-page-no="2">
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="tm-flex">
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-left">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Create an account?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/iXiI6dDNnDA" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-right">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Create an image data set?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/IzDhnYJ0i3M" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="tm-flex">
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-left">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Create a text classification data set?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/EUmx8g5EKdE" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-right">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Create an audio classification data set?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/VJg_A_e6puQ" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="tm-flex">
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-left">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Tag your data?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/RUx6R9L3WJc" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-right">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Add a contributor?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/5BnWOBKvle4" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="tm-flex">
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-left">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Add more data?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/P8mH-6zVHpI" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-right">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Evaluate your data?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/cJEWbn9tzWo" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="tm-flex">
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-left">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Download your data?</h2>
                                            </div>
                                            <div className="media">
                                                <iframe width="520" height="315" src="https://www.youtube.com/embed/XZdFfRvEX5U" frameBorder="0" allowFullScreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="tm-flex">
                                        <div className="col-lg-6 tm-margin-b tm-2-col-left">
                                            <div className="tm-bg-white tm-textbox-padding tm-height-100">
                                                <h2 className="tm-text-title tm-text-title-small tm-header-margin-b">User management</h2>
                                                <p className="tm-text">In order to use Casper-Dataturks you’ll need to create an account, except for the Home and About us pages. To create an account, you need to enter valid e-mail and valid password. After signing up, you’ll receive a verification mail and without verifying your account, you can’t log in. Your info is kept in an MySQL database with hashed password. There are two types of users: admins and annotators. Admins are users that created the project or were given this role by the project creator. Annotator on the other side, as the name suggests, is a member of the project that can only TAG/LABEL data.</p>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 tm-margin-b tm-2-col-right">
                                            <div className="tm-bg-white tm-textbox-padding tm-height-100">
                                                <h2 className="tm-text-title tm-text-title-small tm-header-margin-b">Data sets</h2>
                                                <p className="tm-text">In order to create a data set, you need to be logged in first. After logging in, on the left side there’s a toggled off toolbar with house icon and a plus icon. The house icon brings you to your projects area and the plus icon is used to create a new data set. This project offers the creation of three types of data sets - Image bounding box (for selecting entities from an image), Audio classification and Text classification (which is used for NLP problems). After selecting your type of data set, you are supposed to enter the title of the data set, entities (classes) for your project (for example, for dog breeds data set, you might want to type “Shepard, Poodle”) and the last text box is used for project description. Before submitting your project, check the Advanced options drop down which offers you to select the bounding type (rectangle or polygon). When uploading data, this project supports upload by Zip file and txt file. The txt file should have URLs of the images in each line while the Zip file should contain only the images you’d like to use. After creation, you can start tagging your photos. You do that by clicking Start Tagging. Afterwards you select the class you want to apply to the given picture from the right side and draw it properly on your picture. If you selected Rectangle when creating a project but you want to bound something with a Polygon, you go back to your project, click Options in the right upper corner, click Edit project and in Advanced options change it back to Polygon.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
  );
}
}
