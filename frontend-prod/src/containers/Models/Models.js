import React, { Component } from 'react';
import SystemStats from '../SystemStats/SystemStats';
import { getUidToken } from '../../helpers/dthelper';
import './Models.css';


export default class Models extends Component {

    constructor(props) {
        super(props);
        this.state = {
            projectsInfo: []
          };
        }

    componentWillMount() {
        fetch("http://localhost:8000/projects/" + getUidToken().uid)
        .then(resp => {
            return resp.json();
        })
        .then(resp => {
        // console.log(resp)
        this.setState({projectsInfo: resp});
        console.log(this.state.projectsInfo);
        })
    }

    render() {
        return (
        <div>
            <SystemStats/>
            <div style={{marginTop: '60px'}}>
                <table>
                {this.state.projectsInfo.slice(0).reverse().map(item => (
                    <tr>
                        <td><a key={item.projectId} href={"models/" + item.projectId}>{item.projectTitle}</a></td>
                        <td>{item.problemType}</td>
                        <td>{item.classLabel}</td>
                    </tr>
                ))}
                </table>
            </div>
        </div>
        )
    }
}
