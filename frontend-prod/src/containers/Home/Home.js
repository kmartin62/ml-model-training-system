import React from 'react';
import './Home.css';

export default class Home extends React.Component {
  render() {
  return (
    <section className="hero hero-bg d-flex justify-content-center align-items-center" style={{marginTop: "-100px"}}>
    <div className="container">
         <div className="row">

             <div className="col-lg-6 col-md-10 col-12 d-flex flex-column justify-content-center align-items-center">
                   <div className="hero-text" style={{marginTop: "155px", marginLeft: "40px"}}>

                        <h1 className="text-white" style={{lineHeight: "46px"}}>The future must be defended now</h1>

                        <a href="/projects/login" className="custom-btn btn-bg btn mt-3">Explore now</a>
                   </div>
             </div>

             <div className="col-lg-6 col-12">
               <div className="hero-image">

                 <img src="https://i.imgur.com/9iRDkhf.png" className="img-fluid" alt="working girl" width="700px"/>
               </div>
             </div>

         </div>
    </div>
</section>
  );
}}
