import React from 'react';
import "./AboutUs.css";

export default class AboutUs extends React.Component {
  render() {
  return (
    <ul className="cd-hero-slider">
    <li style={{backgroundColor: '#057a8d'}}>
                    <div className="cd-full-width" style={{marginTop: "-170px", marginLeft: "80px"}}>
                        <div className="container js-tm-page-content tm-section-margin-t" data-page-no="2">
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="tm-flex">
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-left">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">Who are we?</h2>
                                            </div>
                                            <div className="tm-bg-dark-blue text-xs-left tm-textbox tm-textbox-padding tm-white-header-body tm-white-border tm-2-col-equal-height">
                                                <p className="tm-text">Project Casper is designed to protect children from online threats. We are trying to create an artificial supervisor that protects children when they are using the Internet from pornography, nudity, predators, cyber bullying, indoctrination, Internet challenges, involvement in criminal activities, etc. Our project uses artificial intelligence (AI) at the human-computer interaction level, while preserving privacy.</p>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 tm-white-header-container-outer tm-margin-b tm-2-col-right">
                                            <div className="tm-bg-white tm-white-header-container">
                                                <h2 className="tm-text-title tm-text-title-small">What is our goal?</h2>
                                            </div>
                                            <div className="tm-bg-dark-blue text-xs-left tm-textbox tm-textbox-padding tm-white-header-body tm-white-border tm-2-col-equal-height">
                                                <p className="tm-text">The main goal of CASPER is to identify and apply potentials of using aritfical intelligence to protect young people on the Internet. Different types of content are analysed, including text, images, video and audio, as well as the different types of online threats. The resulting system is meant to be modular, extensible, multi-platform, cloud-enabled and compatible with already existing solutions. A special challenge is to support the collaborative use of results while preserving privacy.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="tm-flex">
                                        <div className="col-lg-6 tm-margin-b tm-2-col-left">
                                            <div className="tm-bg-white tm-textbox-padding tm-height-100">
                                                <h2 className="tm-text-title tm-text-title-small tm-header-margin-b">NGI Support</h2>
                                                <p className="tm-text">We found the information about the first <a href="https://wiki.geant.org/pages/viewpage.action?pageId=126979591" target="_blank">NGI Trust call</a> and suggested that we should write a proposal. We were a bit reserved at first, since we were not very experienced in writing proposals for EU-funded projects, and we were afraid that funding would require us to change the research direction. However, the call looked very interesting, as well as the whole NGI initiative, so we decided to give it a try. We are very happy we applied.</p>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 tm-margin-b tm-2-col-right">
                                            <div className="tm-bg-white tm-textbox-padding tm-height-100">
                                                <h2 className="tm-text-title tm-text-title-small tm-header-margin-b">Project Casper</h2>
                                                <p className="tm-text">There are many projects with the same goal – to protect children using the Internet. Also, some of them are utilizing AI. The main characteristics of our project are that it uses AI on the human-computer interaction level, supports different types of content (image, video, audio, text), supports a broad and extensible range of threats, and, maybe most importantly, tries to deeply understand what is happening in the intercepted communications. Additionally, we are exploring the possibility to enable collaboration (e.g. in detecting predators), while preserving the privacy of the user. <a href="https://www.ngi.eu/blog/2020/03/27/whos-ngi-aleksandar-jevremovic-introduces-caspar-to-protect-children-online/" target="_blank">Read our interview</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
  );
}
}
