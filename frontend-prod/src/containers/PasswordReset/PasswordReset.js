import React from 'react';
import './PasswordReset.css';
import { BASE_URL } from '../../helpers/dthelper';

export default class PasswordReset extends React.Component {
    constructor(props) {
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    state = {
        email: "",
        errorMessage: ""
    }

    onInputChange(event) {
        this.setState({email: event.target.value})
    }

    onSubmit() {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(this.state.email)) {
            const requestOptions = {
                method: 'POST'
              };
   
              fetch(BASE_URL + 'resetEmailSend/' + this.state.email, requestOptions)
              .then(async response => {
                if (!response.ok) {
                  this.setState({errorMessage: "Oops, an error occured. You entered an invalid email"})
                  return;
                } else if (response.ok) {
                    this.setState({errorMessage: "An email has been sent"})
                }
              }).catch(error => {
                alert(error)
              })
        } else {
            alert("INVALID EMAIL")
        }
    }


  render() {
  return (
    <div className="container padding-bottom-3x mb-2 mt-5">
        <div className="row justify-content-center">
            <div className="col-lg-8 col-md-10">
                <div className="forgot">
                    <h2>Forgot your password?</h2>
                    <p>Change your password in three easy steps. This will help you to secure your password!</p>
                    <ol className="list-unstyled">
                        <li><span className="text-primary text-medium">1. </span>Enter your email address below.</li>
                        <li><span className="text-primary text-medium">2. </span>Our system will send you a temporary link</li>
                        <li><span className="text-primary text-medium">3. </span>Use the link to reset your password</li>
                    </ol>
                </div>
                {/* <form className="card mt-4" onSubmit={"return false;"}> */}
                    <div className="card-body">
                        <div className="form-group"> <label htmlFor="email-for-pass">Enter your email address</label> <input className="form-control" type="email" id="email-for-pass" required="" onChange={this.onInputChange}/><small className="form-text text-muted">Enter the email address you used during the registration. Then we'll email a link to this address.</small> </div>
                    </div>
                    <div className="card-footer"> <button className="btn btn-success" type = "button" onClick={this.onSubmit}>Get New Password</button> </div>
                {/* </form> */}
            </div>
        </div>
        {this.state.errorMessage}
    </div>
  );
}}
