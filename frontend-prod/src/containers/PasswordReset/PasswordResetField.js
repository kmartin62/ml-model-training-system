import React from 'react';
import { BASE_URL } from '../../helpers/dthelper';

export default class PasswordResetField extends React.Component {
    constructor(props) {
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onConfirmedPasswordChange = this.onConfirmedPasswordChange.bind(this);
    }

    state = {
        password: '',
        confirmedPassword: '',
        errorMessagePw: ''
    }

    onInputChange(event) {
        this.setState({password: event.target.value})
    }

    onConfirmedPasswordChange(event) {
        this.setState({confirmedPassword: event.target.value})
    }

    onSubmit() {
        let re = /^\S{5,}$/;
        if (re.test(this.state.password) && this.state.password === this.state.confirmedPassword) {
            const requestOptions = {
                method: 'POST',
                body: this.state.password
              };
            
              fetch(BASE_URL + 'resetPassword/' + this.props.params.id, requestOptions)
              .then(async response => {
                if (!response.ok) {
                  this.setState({errorMessagePw: "Oops, an error occured."})
                  return;
                } else if (response.ok) {
                    this.setState({errorMessagePw: "Password updated successfully"})
                }
              }).catch(error => {
                alert(error)
              })

        } else {
            this.setState({errorMessagePw: "Invalid password"})
        }
    }


  render() {
  return (
    <section className="mb-5 text-center">

        <p>Set a new password. The password must be 5 characters minimum and must not contain empty spaces</p>

        <form action="#!">
        <div className="md-form md-outline">
            <input type="password" id="newPass" className="form-control" onChange={this.onInputChange}/>
            <label data-error="wrong" data-success="right" htmlFor="newPass">New password</label>
         </div>

        <div className="md-form md-outline">
            <input type="password" id="newPassConfirm" className="form-control" onChange={this.onConfirmedPasswordChange}/>
            <label data-error="wrong" data-success="right" htmlFor="newPassConfirm">Confirm password</label>
        </div>

        <button type="button" className="btn btn-primary mb-4" onClick={this.onSubmit}>Change password</button>
        {this.state.errorMessagePw}
        </form>


  </section>
  );
}}
