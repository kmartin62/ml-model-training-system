import pandas as pd
import numpy as np
import chardet
from scipy.linalg import svd
from numpy import diag

from sklearn.preprocessing import MinMaxScaler, OrdinalEncoder, Normalizer


# dataFrame = pd.read_csv("../heart.csv")


def getDataEncoding(path):
    with open(path, "rb") as raw_data:
        result = chardet.detect(raw_data.read(10000))
    return result['encoding']


def handleInconsistentData(
        df):  # for example, ' Germany' and 'gErManY' should make one country and not two when calling np.unique
    for column in df.columns:
        try:
            df[column] = df[column].str.lower()
            df[column] = df[column].str.strip()
        except:
            continue

def split_data_and_class(df, classLabel, type):
    if classLabel not in list(df.columns):
        print("Class label not in this set")
        return None, None
    else:
        df_return = df.drop(classLabel, axis=1)
        if type == 'classification':
            return df_return, df[classLabel].astype(int)
        else:
            return df_return, df[classLabel]

def calculate_pca_components(df):
    U, s, VT = svd(df)
    Sigma = diag(s)
    sumSigma = 0
    for i in range(len(Sigma)):
        for j in range(len(Sigma[i])):
            sumSigma += Sigma[i][j]

    dictSum = dict()
    for i in range(len(Sigma)):
        for j in range(len(Sigma[i])):
            if i == j and i == 0:
                dictSum[i] = Sigma[i][j]
            elif i == j and i is not 0:
                dictSum[i] = dictSum[i - 1] + Sigma[i][j]

    for i in range(df.shape[1]):
        if dictSum[i] / sumSigma >= 0.99: #99% of the variance is retained
            return i

def encode_data(df):
    enc = OrdinalEncoder()
    for key, value in dict(df.dtypes).items():
        if value == 'object':
            encResult = enc.fit_transform(df[key].values.reshape(-1,1))
            df[key] = encResult

def scale_data(df):
    scaler = MinMaxScaler()
    df[df.columns] = scaler.fit_transform(df[df.columns])

def normalize_data(df):
    transformer = Normalizer()
    df[df.columns] = transformer.fit_transform(df[df.columns])

# if __name__ == '__main__':
#     path = "../heart.csv"
#     encoding = getDataEncoding(path)
#     df = pd.read_csv(path, encoding=encoding)
#     handleInconsistentData(df)
#     print(df)
