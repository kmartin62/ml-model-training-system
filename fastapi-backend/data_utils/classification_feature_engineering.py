from sklearn.feature_selection import mutual_info_classif

def rename_columns(df):
    for i, column in enumerate(list(df.columns)):
        df.rename(columns={column: i}, inplace=True)

def remove_bad_features(df, classLabel):
    rename_columns(df)

    mi = list(enumerate(mutual_info_classif(df, classLabel)))
    thresh = 0
    for key, value in dict(mi).items():
        thresh += value
    thresh = thresh / len(dict(mi))

    for key, value in dict(mi).items():
        if value <= thresh:
            df.drop(key, axis=1, inplace=True)
