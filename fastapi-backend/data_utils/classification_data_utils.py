from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import MinMaxScaler

def drop_duplicates(df):
    df.drop_duplicates(inplace=True)


def delete_single_valued_columns(df, data_conf):
    for key, value in dict(df.nunique()).items():
        if value == 1 or value == len(df):
            df.drop(key, axis=1, inplace=True)
            data_conf["dropped_columns"].append(key)


# deletes columns with more than 40% missing data
def delete_columns_with_missing_data(df, data_conf):
    d = dict((df.isna().sum() / len(df)) * 100)
    for key, value in d.items():
        if value >= 40:
            df.drop(key, axis=1, inplace=True)
            data_conf["dropped_columns"].append(key)

def fill_missing_numericals(df):
    df.fillna(df.mean(), inplace=True)

def fill_missing_categorical(df):
    for key, value in dict(df.dtypes).items():
        if value == 'object':
            df.fillna(value={key: df.mode().iloc[0][key]}, inplace=True)

def encode_data(df):
    enc = OrdinalEncoder()
    for key, value in dict(df.dtypes).items():
        if value == 'object':
            encResult = enc.fit_transform(df[key].values.reshape(-1,1))
            df[key] = encResult

def scale_data(df):
    scaler = MinMaxScaler()
    df[df.columns] = scaler.fit_transform(df[df.columns])

def normalize_data(df):
    transformer = Normalizer()
    df[df.columns] = transformer.fit_transform(df[df.columns])