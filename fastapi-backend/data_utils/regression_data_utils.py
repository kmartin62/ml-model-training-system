from scipy.stats import stats

from . import classification_data_utils
from statsmodels.stats.outliers_influence import variance_inflation_factor
import pandas as pd
import numpy as np

def drop_duplicates(df):
    classification_data_utils.drop_duplicates(df)

def delete_single_valued_columns(df, data_conf):
    classification_data_utils.delete_single_valued_columns(df, data_conf)


# deletes columns with more than 40% missing data
def delete_columns_with_missing_data(df, data_conf):
    classification_data_utils.delete_columns_with_missing_data(df, data_conf)

def fill_missing_numericals(df):
    classification_data_utils.fill_missing_numericals(df)

def fill_missing_categorical(df):
    classification_data_utils.fill_missing_categorical(df)

def encode_data(df):
    classification_data_utils.encode_data(df)

def scale_data(df):
    classification_data_utils.scale_data(df)

def compute_multicoliearity(df):
    vif = pd.DataFrame()
    vif["variables"] = df.columns
    vif["VIF"] = [variance_inflation_factor(df.values, i) for i in range(df.shape[1])]

    return pd.Series(vif.VIF.values, index=vif.variables).to_dict()

def handle_multicolinearity(df, data_conf):
    colinearityDict = compute_multicoliearity(df)
    for i in range(len(colinearityDict)):
        colinearityDict = compute_multicoliearity(df)
        for key, value in colinearityDict.items():
            if value / 10 >= 1.5:
                print(key)
                df.drop(key, axis=1, inplace=True)
                data_conf["dropped_columns"].append(key)
                break

def remove_outliers(df):
    return df[(np.abs(stats.zscore(df)) < 2.5).all(axis=1)]
