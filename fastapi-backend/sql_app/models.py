from datetime import datetime

from sqlalchemy import Column, ForeignKey, Integer, String, Date, MetaData, Table
from sqlalchemy.orm import relationship

from .database import Base, engine

"""
ONE TO MANY RELATIONSHIP

Ako foreign key e vo prvata klasa togas imame MANY-TO-ONE
Ako foreign key e vo vtorata klasa togas imame ONE-TO-MANY
MANY-TO-MANY bi znacelo foreign key i vo dvete klasi ili nova tabela so IDs od ovie klasi
i posle ID do ovaa tabela
ONE-TO-ONE bi znacelo foreign key vo vtorata klasa i uselist=False vo relationship() na prvata klasa
"""

class User(Base):
    __table__ = Table('d_users', Base.metadata, autoload=True, autoload_with=engine)


class DataInfo(Base):
    __table__ = Table('d_data_info', Base.metadata, autoload=True, autoload_with=engine)

    # __tablename__ = "d_data_info"
    # id = Column(Integer, primary_key=True, index=True)
    # isURL = Column(Integer, index=True)
    # dataSource = Column(String(100), index=True)
    # dateCreated = Column(Date, index=True, default=datetime.date(datetime.now()))
    # projectId = Column(Integer, ForeignKey("d_ml_projects.projectId"))

    # data_for_project = relationship("Project", back_populates="data")


class Project(Base):
    __table__ = Table('d_ml_projects', Base.metadata, autoload=True, autoload_with=engine)

    # __tablename__ = "d_ml_projects"

    # projectId = Column(Integer, primary_key=True)
    # projectTitle = Column(String(200))
    # projectDescription = Column(String(550), index=True)
    # problemType = Column(String(15), index=True)
    # classLabel = Column(String(100), index=True)

    # userId = Column(String(100), ForeignKey("d_users.oAuthId"))
    # data = relationship("DataInfo", back_populates="data_for_project")
