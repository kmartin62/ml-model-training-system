from typing import List, Optional

from pydantic import BaseModel


class ProjectBase(BaseModel):
    projectTitle: str
    projectDescription: Optional[str] = None
    problemType: str
    classLabel: str


class ProjectCreate(ProjectBase):
    pass


class Project(ProjectBase):
    projectId: int
    userId: str

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    firstName: str
    secondName: str
    city: Optional[str] = None


class User(UserBase):
    id: str

    class Config:
        orm_mode = True

class DataInfoBase(BaseModel):
    isURL: int
    dataSource: str
    training_time: str
    num_rows: int
    num_features: int
    data_scaled: int


class DataInfoCreate(DataInfoBase):
    pass


class DataInfo(DataInfoBase):
    id: int
    projectId: int

    class Config:
        orm_mode = True
