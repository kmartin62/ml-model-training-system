import json

from sqlalchemy.orm import Session
from . import models, schemas
import pandas as pd
from data_utils import classification_data_utils
from data_utils import regression_data_utils
from data_utils import data_cleaning
from models.classification_models import CModels
from models.regression_models import RModels
import time
import datetime

import pybreaker
from circuitbreaker.CircuitBreakerListener import CircuitBreakerListener

from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split

from mongo.services import insertMetrics

import pickle

db_breaker = pybreaker.CircuitBreaker(fail_max=1, reset_timeout=5, listeners=[CircuitBreakerListener()])


@db_breaker
def get_user(db: Session, user_id: str):
    return db.query(models.User).filter(models.User.oAuthId == user_id).first()


def getUserId(db: Session, user_id: str):
    return get_user(db, user_id).id


@db_breaker
def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()


def create_project(db: Session, project: schemas.ProjectCreate, oAuthId: str, df: pd.DataFrame):
    data_conf = {"dropped_columns": list(), "scaled_data": 1, "pca_components": None, "n_estimators": None,
                 "classLabel": None}
    user_id = get_user(db, oAuthId).id
    print(user_id)
    metricsDict = {}
    try:
        data_conf["classLabel"] = str(project.classLabel)
        db_project = models.Project(**project.dict(), userId=user_id)
        db.add(db_project)
        db.commit()
        db.refresh(db_project)
        db_projectId = db_project.projectId
        metricsDict['projectId'] = db_projectId
        t0 = time.time()
        if db_project.problemType == 'classification':
            classification_data_utils.drop_duplicates(df)
            classification_data_utils.delete_single_valued_columns(df, data_conf)
            classification_data_utils.delete_columns_with_missing_data(df, data_conf)
            classification_data_utils.fill_missing_numericals(df)
            classification_data_utils.fill_missing_categorical(df)
            classification_data_utils.encode_data(df)
            data_cleaning.handleInconsistentData(df)

            df_x, df_y = data_cleaning.split_data_and_class(df, str(project.classLabel), db_project.problemType)

            df_scaled = df_x.copy()
            df_normalized = df_x.copy()

            classification_data_utils.scale_data(df_scaled)
            classification_data_utils.normalize_data(df_normalized)

            '''Move these inside a function'''
            pca_scaled = PCA(n_components=data_cleaning.calculate_pca_components(df_scaled))
            principalComponents_scaled = pca_scaled.fit_transform(df_scaled)
            principalDf_scaled = pd.DataFrame(data=principalComponents_scaled)

            pca_normalized = PCA(n_components=data_cleaning.calculate_pca_components(df_normalized))
            principalComponents_normalized = pca_normalized.fit_transform(df_normalized)
            principalDf_normalized = pd.DataFrame(data=principalComponents_normalized)
            ''''''

            X_train, X_test, y_train, y_test = train_test_split(principalDf_scaled, df_y, test_size=0.20,
                                                                random_state=42)

            model = CModels()

            model.train_logistic_regression(X_train, y_train, X_test, y_test, metricsDict)
            model.train_knn_classifier(X_train, y_train, X_test, y_test, metricsDict)
            model.train_random_forest_classifier(X_train, y_train, X_test, y_test, metricsDict)
            model.train_svm_classifier(X_train, y_train, X_test, y_test, metricsDict)

            X_train, X_test, y_train, y_test = train_test_split(principalDf_normalized, df_y, test_size=0.20,
                                                                random_state=42)
            model.train_naive_bayes_classifier(X_train, y_train, X_test, y_test, metricsDict)
            model.train_lda_classifier(X_train, y_train, X_test, y_test, metricsDict)



            print(model.get_scores())
            print(model.get_best_model())
            if str(model.get_best_model()) == 'LinearDiscriminantAnalysis()' or str(
                    model.get_best_model()) == 'GaussianNB()':
                data_conf["scaled_data"] = 0
                data_conf["pca_components"] = data_cleaning.calculate_pca_components(df_normalized)
            else:
                data_conf["pca_components"] = data_cleaning.calculate_pca_components(df_scaled)

            data_conf["best_model"] = str(model.get_best_model())
            with open("conf_files/" + str(user_id) + '_' + str(db_projectId) + ".json", "w") as outfile:
                json.dump(data_conf, outfile)

            filename = str(user_id) + '_' + str(db_projectId) + '.sav'
            pickle.dump(model.get_best_model(), open('saved_models/' + filename, 'wb'))
        else:
            print(project)
            regression_data_utils.drop_duplicates(df)
            regression_data_utils.delete_single_valued_columns(df, data_conf)
            regression_data_utils.delete_columns_with_missing_data(df, data_conf)
            regression_data_utils.fill_missing_numericals(df)
            regression_data_utils.fill_missing_categorical(df)
            regression_data_utils.encode_data(df)
            regression_data_utils.scale_data(df)

            df = regression_data_utils.remove_outliers(df)

            df_x, df_y = data_cleaning.split_data_and_class(df, str(project.classLabel), db_project.problemType)

            regression_data_utils.handle_multicolinearity(df_x, data_conf)

            pca = PCA(n_components=data_cleaning.calculate_pca_components(df_x))
            principalComponents = pca.fit_transform(df_x)
            principalDf = pd.DataFrame(data=principalComponents)

            data_conf["pca_components"] = data_cleaning.calculate_pca_components(df_x)

            X_train, X_test, y_train, y_test = train_test_split(principalDf, df_y, test_size=0.20,
                                                                random_state=42)

            model = RModels()
            final = model.get_final_model(X_train, y_train, X_test, y_test, data_conf, metricsDict)

            data_conf["best_model"] = str(final)

            with open("conf_files/" + str(user_id) + '_' + str(db_projectId) + ".json", "w") as outfile:
                json.dump(data_conf, outfile)

            filename = str(user_id) + '_' + str(db_projectId) + '.sav'
            pickle.dump(final, open('saved_models/' + filename, 'wb'))
        t1 = time.time()
        insertMetrics(metricsDict)
        print(data_conf)
        db_datainfo = models.DataInfo(isURL=1, dataSource='file', num_rows=len(df), num_features=len(df.columns),
                                      data_scaled=data_conf["scaled_data"] == 1, training_time=str(t1-t0),
                                      dateTimeCreated=str(datetime.datetime.now()), projectId=db_projectId)
        db.add(db_datainfo)
        db.commit()
        db.refresh(db_datainfo)

    except Exception as e:
        db_project = None
        print(f"Error occured: {e}")

    return db_project


@db_breaker
def get_projects(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Project).offset(skip).limit(limit).all()


@db_breaker
def get_project(user_id: str, db: Session):
    user = get_user(db, user_id)
    user_id = user.id
    return db.query(models.Project).filter(models.Project.userId == user_id).all()


@db_breaker
def get_project_by_id(projectId: int, db: Session):
    return db.query(models.Project).filter(models.Project.projectId == projectId).first()


@db_breaker
def get_project_info(projectId: int, db: Session):
    try:
        return db.query(models.DataInfo).filter(models.DataInfo.projectId == projectId).first()
    except:
        return {"Error": "The model is still training"}
