import os
from asyncio import sleep
from typing import List
import json
import pandas as pd
import pickle

from fastapi import FastAPI, File, UploadFile, Depends, HTTPException, WebSocket
from fastapi.middleware.cors import CORSMiddleware
from sklearn.decomposition import PCA

from sqlalchemy.orm import Session

from sql_app import crud, models, schemas
from sql_app.database import SessionLocal, engine
from utils.FileUploader import Data
from utils.SystemPerformances import getSystemPerformances

from sql_app.crud import db_breaker
from circuitbreaker.cb_call import circuitbreaker_call
from data_utils import data_cleaning

from mongo.services import getMetricsByProjectId

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
data = Data()
predicted_data = Data()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.websocket('/ws')
async def webSocket(websocket: WebSocket):
    print('CLIENT CONNECTED')
    await websocket.accept()
    while True:
        cpu, ram, gpu = getSystemPerformances()
        try:
            resp = {'cpu': cpu, 'ram': ram, 'gpu': gpu}
            await websocket.send_json(resp)
        except Exception as e:
            print(e)
            break
        await sleep(1)
    print('CLIENT DISCONNECTED')


@app.get("/users/", response_model=List[schemas.User])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return circuitbreaker_call(db_breaker, crud.get_users(db, skip=skip, limit=limit))


@app.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return circuitbreaker_call(db_breaker, db_user)


@app.post("/projects", response_model=schemas.Project)
def create_project(user_id: str, project: schemas.ProjectCreate, db: Session = Depends(get_db)):
    return crud.create_project(db=db, project=project, oAuthId=user_id, df=data.getData())


@app.get("/projects", response_model=List[schemas.Project])
def get_projects(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return circuitbreaker_call(db_breaker, crud.get_projects(db, skip=skip, limit=limit))

@app.get("/projects/{user_id}", response_model=List[schemas.Project])
def get_project_by_user(user_id: str, db: Session = Depends(get_db)):
    return circuitbreaker_call(db_breaker, crud.get_project(user_id, db))

@app.get("/project/{projectId}", response_model=schemas.Project)
def get_project_by_id(projectId: int, db: Session = Depends(get_db)):
    return circuitbreaker_call(db_breaker, crud.get_project_by_id(projectId, db))

@app.get("/project_info/{projectId}", response_model=schemas.DataInfo)
def get_project_info(projectId: int, db: Session = Depends(get_db)):
    return circuitbreaker_call(db_breaker, crud.get_project_info(projectId, db))

@app.get("/metrics")
def get_columns(projectId: int):
    return getMetricsByProjectId(projectId)

@app.get("/configuration")
def get_columns(UID: str, projectId: int, db: Session = Depends(get_db)):
    userId = crud.getUserId(db, UID)
    fileName = userId + '_' + str(projectId)
    with open("conf_files/" + fileName + '.json') as json_file:
        dataInfo = json.load(json_file)

    return {'dropped_columns': dataInfo['dropped_columns'], 'pca_components': dataInfo['pca_components'],
            'best_model': dataInfo['best_model']}

@app.get("/columns")
def get_columns():
    try:
        print(list(data.getData().columns))
        return {"data": list(data.getData().columns)}
    except:
        raise HTTPException(status_code=404, detail="Invalid dataset")

@app.get("/predict")
def predict_data(UID: str, projectId: int, db: Session = Depends(get_db)):
    df = predicted_data.getData()
    userId = crud.getUserId(db, UID)
    print(userId)
    fileName = userId + '_' + str(projectId)
    with open("conf_files/" + fileName + '.json') as json_file:
        dataInfo = json.load(json_file)

    print(dataInfo)

    df.drop(dataInfo['dropped_columns'], axis=1)
    data_cleaning.encode_data(df)
    if dataInfo['scaled_data'] == 1:
        data_cleaning.scale_data(df)
    else:
        data_cleaning.normalize_data(df)

    pca = PCA(n_components=dataInfo['pca_components'])
    principalComponents = pca.fit_transform(df)
    principalDf = pd.DataFrame(data=principalComponents)

    print(principalDf)

    loaded_model = pickle.load(open("saved_models/" + fileName + ".sav", 'rb'))
    predictions = loaded_model.predict(principalDf)

    print(predictions)
    listPredictions = list(predictions)
    print(type(listPredictions))
    return {"predicted_data": predictions.tolist()}


@app.post("/uploadfile")
async def create_upload_file(to_predict: int, file: UploadFile = File(...)):
    if to_predict == 1:
        predicted_data.uploadData(file)
    else:
        data.uploadData(file)
    return {"INFO": f"{file.filename} uploaded successfully"}


@app.get("/")
async def root():
    return circuitbreaker_call(db_breaker, {"message": "Hello world"})


if __name__ == '__main__':
    os.system("uvicorn main:app --reload-dir /home/kmartin62/Desktop/ml-model-training-system/fastapi-backend/main.py "
              "--timeout-keep-alive 2")
