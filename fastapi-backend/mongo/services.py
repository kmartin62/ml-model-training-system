import pymongo


def getClient():
    client = pymongo.MongoClient(
        "mongodb+srv://kmartin62:SNeSptwFAQkNUBjB@metrics-cluster.xctpa.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
    return client


db = getClient().modelMetrics


def insertMetrics(metricsDict):
    metrics = db.metrics
    metrics.insert_one(metricsDict)


def getMetricsByProjectId(projectId):
    metrics = db.metrics
    return metrics.find_one({"projectId": projectId}, {'_id': 0, 'projectId': 0})
