from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier

from sklearn.naive_bayes import GaussianNB  # Normalized dataset
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis  # Normalized dataset

from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score


class CModels:
    def __init__(self):
        self.scoreDict = dict()
        self.lr = LogisticRegression()
        self.knn = KNeighborsClassifier()
        self.svm = SVC()
        self.rfc = RandomForestClassifier()
        self.nb = GaussianNB()
        self.lda = LinearDiscriminantAnalysis()

    def calculateMetrics(self, X_test, y_test, model):
        localMetrics = dict()

        score = model.score(X_test, y_test)
        localMetrics["score"] = score
        localMetrics["F1_macro"] = f1_score(y_test, model.predict(X_test), average='macro')
        localMetrics["F1_micro"] = f1_score(y_test, model.predict(X_test), average='micro')
        localMetrics["accuracy_score"] = accuracy_score(y_test, model.predict(X_test))
        localMetrics["precision_score"] = precision_score(y_test, model.predict(X_test), average='micro')
        localMetrics["recall_score"] = recall_score(y_test, model.predict(X_test), average='micro')

        return localMetrics

    def get_scores(self):
        return self.scoreDict

    def get_best_model(self):
        sortedDict = {key: value for key, value in sorted(self.scoreDict.items(), key=lambda item: item[1], reverse=True)}
        return list(sortedDict.keys())[0]

    def train_logistic_regression(self, X_train, y_train, X_test, y_test, metricsDict):
        self.lr.fit(X_train, y_train)
        score = self.lr.score(X_test, y_test)
        self.scoreDict[self.lr] = score

        metricsDict[str(self.lr)] = self.calculateMetrics(X_test, y_test, self.lr)

    def train_knn_classifier(self, X_train, y_train, X_test, y_test, metricsDict):
        self.knn.fit(X_train, y_train)
        score = self.knn.score(X_test, y_test)
        self.scoreDict[self.knn] = score

        metricsDict[str(self.knn)] = self.calculateMetrics(X_test, y_test, self.knn)

    def train_svm_classifier(self, X_train, y_train, X_test, y_test, metricsDict):
        self.svm.fit(X_train, y_train)
        score = self.svm.score(X_test, y_test)
        self.scoreDict[self.svm] = score

        metricsDict[str(self.svm)] = self.calculateMetrics(X_test, y_test, self.svm)

    def train_random_forest_classifier(self, X_train, y_train, X_test, y_test, metricsDict):
        self.rfc.fit(X_train, y_train)
        score = self.rfc.score(X_test, y_test)
        self.scoreDict[self.rfc] = score

        metricsDict[str(self.rfc)] = self.calculateMetrics(X_test, y_test, self.rfc)

    def train_naive_bayes_classifier(self, X_train, y_train, X_test, y_test, metricsDict):
        self.nb.fit(X_train, y_train)
        score = self.nb.score(X_test, y_test)
        self.scoreDict[self.nb] = score

        metricsDict[str(self.nb)] = self.calculateMetrics(X_test, y_test, self.nb)

    def train_lda_classifier(self, X_train, y_train, X_test, y_test, metricsDict):
        self.lda.fit(X_train, y_train)
        score = self.lda.score(X_test, y_test)
        self.scoreDict[self.lda] = score

        metricsDict[str(self.lda)] = self.calculateMetrics(X_test, y_test, self.lda)
