from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
from sklearn.linear_model import ElasticNet
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor  # Needs n_estimators
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import GradientBoostingRegressor  # Needs n_estimators

from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.model_selection import KFold

import numpy as np

from sklearn.metrics import max_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_squared_log_error
from sklearn.metrics import median_absolute_error
from sklearn.metrics import r2_score


class RModels:

    def __init__(self):
        self.models = [LinearRegression(), Lasso(), ElasticNet(), DecisionTreeRegressor(),
                       KNeighborsRegressor()]
        self.lr = LinearRegression()
        self.la = Lasso()
        self.en = ElasticNet()
        self.dt = DecisionTreeRegressor()
        self.knn = KNeighborsRegressor()

        self.rf = None
        self.gb = None

        self.cv = dict()

    def evaluate_best_model(self, df_x, df_y):
        best_score = None
        best_model = ''
        for model in self.models:
            kfold = KFold(n_splits=10)
            cv_results = cross_val_score(model, df_x, df_y, cv=kfold, scoring='neg_mean_squared_error')
            if best_score is None:
                best_score = cv_results.mean()
                best_model = model
            elif cv_results.mean() > best_score:
                best_score = cv_results.mean()
                best_model = model
            self.cv[str(model)] = cv_results.mean()
            print(model, cv_results.mean(), cv_results.std())
        return best_model

    def compute_n_estimators(self, model, df_x, df_y):
        param_grid = dict(n_estimators=np.array([50, 100, 200, 300, 400]))
        kfold = KFold(n_splits=10)
        grid = GridSearchCV(estimator=model, param_grid=param_grid, scoring='neg_mean_squared_error', cv=kfold)
        grid_result = grid.fit(df_x, df_y)
        d = grid_result.best_params_
        return list(d.values())[0]

    def calculateMetrics(self, X_test, y_test, model):
        localDict = dict()

        localDict["max_error"] = max_error(y_test, model.predict(X_test))
        localDict["mean_absolute_error"] = mean_absolute_error(y_test, model.predict(X_test))
        localDict["mean_squared_error"] = mean_squared_error(y_test, model.predict(X_test))
        localDict["root_mean_squared_error"] = mean_squared_error(y_test, model.predict(X_test)) ** 0.5
        localDict["mean_squared_log_error"] = mean_squared_log_error(y_test, model.predict(X_test))
        localDict["median_absolute_error"] = median_absolute_error(y_test, model.predict(X_test))
        localDict["r2_score"] = r2_score(y_test, model.predict(X_test))
        localDict["cross_val_score"] = self.cv[str(model)]

        return localDict

    def get_final_model(self, X_train, y_train, X_test, y_test, data_conf, metricsDict):
        best_model = str(self.evaluate_best_model(X_train, y_train))

        self.train_linear_regression(X_train, y_train, X_test, y_test, metricsDict)
        self.train_lasso_regression(X_train, y_train, X_test, y_test, metricsDict)
        self.train_elasticnet_regression(X_train, y_train, X_test, y_test, metricsDict)
        self.train_decision_tree_regression(X_train, y_train, X_test, y_test, metricsDict)
        self.train_knn_regression(X_train, y_train, X_test, y_test, metricsDict)
        # self.train_random_forest_regression(X_train, y_train, X_test, y_test, metricsDict,
        #                                     self.compute_n_estimators(RandomForestRegressor(), X_train, y_train))

        # self.train_gradient_boost_regression(X_train, y_train, X_test, y_test, metricsDict,
        #                                      self.compute_n_estimators(GradientBoostingRegressor(), X_train,
        #                                                                y_train))

        if best_model == 'LinearRegression()':
            return self.lr
        elif best_model == 'Lasso()':
            return self.la
        elif best_model == 'ElasticNet()':
            return self.en
        elif best_model == 'DecisionTreeRegressor()':
            return self.dt
        elif best_model == 'KNeighborsRegressor()':
            return self.knn
        elif best_model == 'RandomForestRegressor()':
            data_conf["n_estimators"] = self.compute_n_estimators(RandomForestRegressor(), X_train, y_train)
            return self.rf
        else:
            data_conf["n_estimators"] = self.compute_n_estimators(RandomForestRegressor(), X_train, y_train)
            return self.gb

    def train_linear_regression(self, X_train, y_train, X_test, y_test, metricsDict):
        self.lr.fit(X_train, y_train)
        metricsDict[str(self.lr)] = self.calculateMetrics(X_test, y_test, self.lr)

    def train_lasso_regression(self, X_train, y_train, X_test, y_test, metricsDict):
        self.la.fit(X_train, y_train)
        metricsDict[str(self.la)] = self.calculateMetrics(X_test, y_test, self.la)

    def train_elasticnet_regression(self, X_train, y_train, X_test, y_test, metricsDict):
        self.en.fit(X_train, y_train)
        metricsDict[str(self.en)] = self.calculateMetrics(X_test, y_test, self.en)

    def train_decision_tree_regression(self, X_train, y_train, X_test, y_test, metricsDict):
        self.dt.fit(X_train, y_train)
        metricsDict[str(self.dt)] = self.calculateMetrics(X_test, y_test, self.dt)

    def train_knn_regression(self, X_train, y_train, X_test, y_test, metricsDict):
        self.knn.fit(X_train, y_train)
        metricsDict[str(self.knn)] = self.calculateMetrics(X_test, y_test, self.knn)

    def train_random_forest_regression(self, X_train, y_train, X_test, y_test, metricsDict, n):
        self.rf = RandomForestRegressor(n_estimators=n)
        self.rf.fit(X_train, y_train)
        metricsDict[str(self.rf)] = self.calculateMetrics(X_test, y_test, self.rf)

    def train_gradient_boost_regression(self, X_train, y_train, X_test, y_test, metricsDict, n):
        self.gb = GradientBoostingRegressor(n_estimators=n)
        self.gb.fit(X_train, y_train)
        metricsDict[str(self.gb)] = self.calculateMetrics(X_test, y_test, self.gb)

