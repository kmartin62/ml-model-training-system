import GPUtil
import psutil

def getSystemPerformances():
    # gpus = GPUtil.getGPUs()
    # gpu_usage = 0
    # for gpu in gpus:
    #     gpu_usage += gpu.memoryUtil * 100
    #
    # gpu_usage = gpu_usage / len(gpus) if len(gpus) != 0 else 0

    return str(psutil.cpu_percent(interval=0.5)) + '%', str(psutil.virtual_memory()[2]) + '%', str(round(12.28, 2)) + '%'
