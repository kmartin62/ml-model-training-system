import pandas as pd
from fastapi import HTTPException
import os
import time

def saveDataLocally(file):
    with open(file.filename, "wb") as buffer:
        buffer.write(file.file.read())


class Data:
    def __init__(self):
        self.data = None

    def uploadData(self, file):
        if file.filename.endswith("txt"):
            try:
                self.data = pd.read_csv(str(file.file.read())[2:-3])
                # print(self.data)
            except:
                print("Invalid csv")
                self.data = None
        elif file.filename.endswith("csv") or file.filename.endswith("xslx"):
            saveDataLocally(file)
            self.data = pd.read_csv(file.filename)

            time.sleep(3)
            os.remove(file.filename)
            # print(self.data)
        else:
            raise HTTPException(status_code=404, detail="Invalid dataset")

    def getData(self):
        return self.data
