import React, { Component } from 'react';
import './UploadFile.css';

export default class UploadFile extends Component{
  constructor(props){
    super(props);
    this.state = {
        selectedFile: null,
        errMsg: ''
      };
    }

    sleep = (ms) => {
      return new Promise(resolve => setTimeout(resolve, ms));
    }
    
      onFileChange = async event => {
        this.setState({selectedFile: event.target.files[0]});
        await this.sleep(1000);
        this.onFileUpload();
      };
    
      onFileUpload = () => {
        const URL = 'http://localhost:8000';
        const formData = new FormData();
    
        formData.append(
          "file",
          this.state.selectedFile,
          this.state.selectedFile.name,
        );

        if(this.props.toPredict === 0){
    
        fetch(URL + "/uploadfile?to_predict=" + this.props.toPredict, {
          method: "POST",
          body: formData
        })
        .then(result => {
          if (result.status !== 200) {
              this.setState({errMsg: 'Invalid file'});
          }

          if (result.status === 200) {
            this.setState({errMsg: ''})
             fetch(URL + "/columns")
            .then(resp => {
              return resp.json();
            })
            .then(resp => {
              console.log(resp.data);
              this.props.onUploadCSV(resp.data);
            });
          }
        });
      } else {
        fetch(URL + "/uploadfile?to_predict=" + this.props.toPredict, {
          method: "POST",
          body: formData
        })
        .then(result => {
          if (result.status !== 200) {
              this.setState({errMsg: 'Invalid file'});
          }

          if (result.status === 200) {
            this.setState({errMsg: ''})
             fetch(URL + "/predict?userId=ff808181795644b80179565ff4570000&projectId=57")
            .then(resp => {
              return resp.json();
            })
            .then(resp => {
              this.props.onUploadCSV(resp.predicted_data);
            });
          }
        });
      }
      };
    

      render() {
        return (
          <div className="container">
          <form className="form">
            <div className="file-upload-wrapper" data-text="Select your file!">
              <input name="file-upload-field" type="file" class="file-upload-field" onChange={this.onFileChange} value=""/>
            </div>
          </form>
        </div>
            
        //   <div>
        //     <div>
        //       <input type="file" onChange={this.onFileChange} required/>
        //     </div>
        //     {this.state.errMsg}
        //  </div> 
        );
      }
}