import React, { Component } from 'react';
import ModelData from '../ModelData/ModelData'
import SystemStats from '../SystemStats/SystemStats'
import './App.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

import DataTable from 'react-data-table-component';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
        columns: 0,
        dataSetLength: 0,
        trainingTime: '',
        droppedColumns: [],
        bestModel: '',
        pca_components: 0
      };
    }

  
  
  componentDidMount(){
    fetch("http://localhost:8000/project_info/99")
    .then(resp => {
      return resp.json();
    })
    .then(resp => {
      this.setState({columns: resp.num_features, dataSetLength: resp.num_rows, trainingTime: resp.training_time})
    })

    fetch("http://localhost:8000/configuration?UID=IoGPGa8nk65fNv8k84x644kmskQS&projectId=98")
    .then(resp => {
      return resp.json();
    })
    .then(resp => {
      this.setState({droppedColumns: resp.dropped_columns, bestModel: resp.best_model, pca_components: resp.pca_components})
      console.log(resp)
    })
  }

  render() { 

    return(
      <div>
          <div>Total number of columns: {this.state.columns}</div>
          <div>Total number of rows in dataset: {this.state.dataSetLength}</div>
          <div>Time spent on model training: {this.state.trainingTime.substring(0, 5)} seconds</div><br/>
          <div>Number of Principal Component Analysis (PCA) components: {this.state.pca_components}</div>
          <div>The best selected model by the analysis is <b>{this.state.bestModel.substring(0, this.state.bestModel.length - 2)}</b></div>
          <div>Dropped/unused columns:</div>
          <ol>
            {this.state.droppedColumns.map((data, index) => (
              <li key={index}>{data}</li>
            ))}
          </ol>
      </div>
    );
  
  }
}

export default App;
