import React, { Component } from 'react';
import './SystemStats.css';

export default class SystemStats extends Component{
    constructor(props) {
        super(props);
        this.state = {cpu: '', gpu: '', ram: ''};
        this.changeState = this.changeState.bind(this);
    }

    changeState = (data) => {
        this.setState({cpu: data.cpu, gpu: data.gpu, ram: data.ram});
    }

    componentDidMount() {
        const ws = new WebSocket('ws://localhost:8000/ws')
        this.setState({ws: ws})
        let that = this;
        ws.onmessage = function (event) {
          let data = JSON.parse(event.data);
          that.changeState(data);
        }
    }
    
    componentWillUnmount() {
        const {ws} = this.state;
        ws.close();
    }

    render() {
        return(
        <div class ="wrapper">
            <p>CPU: {this.state.cpu}   GPU: {this.state.gpu}   RAM: {this.state.ram}</p>
        </div>
        )
    }
}