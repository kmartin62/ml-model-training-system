import React, { Component } from 'react';
import UploadFile from '../UploadFile/UploadFile';
import { Button, Checkbox, Form, Dropdown } from 'semantic-ui-react';
import './ModelData.css';

export default class ModelData extends Component {
  constructor(props){
    super(props);

    this.state = {
      cpu: '', 
      gpu: '', 
      ram: '', 
      value: '', 
      title: '', 
      class: '', 
      description: null, 
      columns: [], 
      options: [], 
      predictedData: [],
      errMsg: '',
      infoMsg: '',
      listItems: null};

    this.changeState = this.changeState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.sendToBackend = this.sendToBackend.bind(this);
    this.handleColumns = this.handleColumns.bind(this);
    this.getPredictions = this.getPredictions.bind(this);
  }

  handleColumns = (columnsFromCSV) => {
      const optionsArray = []
      this.setState({columns: columnsFromCSV});
      this.state.columns.forEach((value, index) => {
        console.log(value, index);
        optionsArray.push({key: index, text: value, value: value});
      });
      this.setState({options: optionsArray});
  }

  getPredictions = (predictions) => {
    const optionsArray = []
    this.setState({predictedData: predictions});
}

  async sendToBackend(e) {
    e.preventDefault();

    if(this.state.class === '' || this.state.value === ''){
      this.setState({errMsg: 'Please select class label/problem type'});
    } else {
      this.setState({errMsg: ''});
    await fetch("http://127.0.0.1:8000/projects?user_id=IoGPGa8nk65fNv8k84x644kmskQS", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({projectTitle: this.state.title, 
            projectDescription: this.state.description, problemType: this.state.value, classLabel: this.state.class})
        });
    }

    this.setState({infoMsg: 'Please go to Models section'});

  }

  updateTitleValue(e) {
    e.preventDefault();
    this.setState({
      title: e.target.value
    });
  }

  updateDescriptionValue(e) {
    e.preventDefault();
    this.setState({
      description: e.target.value
    });
  }

  handleChange = (e, { value }) => this.setState({ value })

  handleOnChangeDropdown = async (e, data) => {
    this.setState({class: data.value});
  }


  changeState = (data) => {
    this.setState({cpu: data.cpu, gpu: data.gpu, ram: data.ram});
  }
  
  render() {
    const { value } = this.state;
    console.log(this.state.predictedData);
    const listItems = this.state.predictedData.map((data, index) => 
      <li key={index}>{data}</li>
    );

    return(
      <div>
      <Form onSubmit={this.sendToBackend} className="ui-form">
      <Form.Field>
        <label>Project title</label>
        <input placeholder='Enter your project title' onChange={e => this.updateTitleValue(e)} required/>
      </Form.Field>

      <UploadFile onUploadCSV={this.getPredictions} toPredict={1}/>

      <Dropdown placeholder="Class" clearable options={this.state.options} selection required onChange={this.handleOnChangeDropdown} className="ui-selection"/>

      <Form.Group inline required style={{marginTop: "10px", marginLeft: "120px"}}>
          <label>Type</label>
          <Form.Radio
            label='Regression'
            value='regression'
            checked={value === 'regression'}
            onChange={this.handleChange}
          />
          <Form.Radio
            label='Classification'
            value='classification'
            checked={value === 'classification'}
            onChange={this.handleChange}
            required
          />
     </Form.Group>

     <Form.TextArea label='Project description' placeholder='(Optional) Describe your project' onChange={e => this.updateDescriptionValue(e)} style={{height: "150px"}}/>
      {/* <Form.Field>
        <Checkbox label='Notify me on mail when my project will be trained' />
      </Form.Field> */}
      <Button type='submit'>Submit</Button>
    </Form>

      {this.state.errMsg}
      {this.state.infoMsg}
      {listItems}
    </div>

    )
  }
}
